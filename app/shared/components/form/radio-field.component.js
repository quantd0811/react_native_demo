import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Item, Input, Text, Label, Icon } from 'native-base';
import { color, labelForm } from '../../utils/variables';
import { CheckBox } from 'react-native-elements';

class RadioField extends React.Component {
    constructor(props) {
        super(props);
        this.onValueChange = props.onValueChange;
        this.state = {
            name: props.name,
            value: props.value,
            items: props.items,
            errors: [],
            valid: true,
            validators: props.validators || {},
            direction: props.direction || 'horizontal'
        }
    }

    componentWillReceiveProps(props) {
        this.setState({
            value: props.value
        })
    }

    valid() {
        return this.state.errors.length == 0;
    }

    validate() {
        debugger
        let valid = true;
        let errors = [];

        this.setState({
            valid: valid,
            errors: errors
        })
    }

    getStyle() {
        return {

        }
    }

    getDirectionStyle() {
        return {
            flexDirection: this.state.direction == 'horizontal' ? 'row' : 'column'
        }
    }

    render() {
        debugger
        return (
            <View style={{ borderBottomColor: color.formControlBorderColor, borderBottomWidth: 0.5, paddingTop: 5 }}>
                <Text style={{ ...labelForm, marginLeft: 10 }}>{this.state.name}</Text>
                <View style={this.getDirectionStyle()}>
                    {this.state.items.map(e => (
                        <CheckBox
                            checkedIcon={<Icon name="circle-slice-8" type="MaterialCommunityIcons" style={styles.iconSelected}></Icon>}
                            uncheckedIcon={<Icon name="circle-outline" type="MaterialCommunityIcons" style={styles.iconSelected}></Icon>}
                            containerStyle={{
                                borderWidth: 0,
                                backgroundColor: 'transparent',
                                paddingTop: 0, paddingBottom: 0,
                                paddingLeft: 0, paddingRight: 0
                            }}
                            title={e.name}
                            checked={this.state.value == e.value}
                            onPress={() => this.setState({ value: e.value })}
                        />
                    ))}
                </View>
                {this.state.errors.map(error => (
                    <Text style={styles.errorMessageColor}>{error}</Text>
                ))}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    errorMessageColor: {
        color: color.danger
    },
    iconSelected: {
        color: '#3f51b5'
    },
})

export default RadioField;