import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, TouchableWithoutFeedback, Dimensions } from 'react-native';
import { List, ListItem, Left, Right, Icon, Button, Content, Container, Item } from 'native-base';
import Modal from 'react-native-modal';
import { ScrollView, FlatList } from 'react-native-gesture-handler';
import { color } from '../../utils/variables';

class ListArrayField extends React.Component {
    constructor(props) {
        super(props);
        this.renderIcon = props.renderIcon;
        this.idKey = props.idKey || 'id';
        this.textKey = props.textKey || 'text';
        this.subtitleKey = props.subtitleKey || 'subtitle';
        this.onChange = props.onChange;
        this.state = {
            title: props.title,
            mode: props.mode,
            useModal: props.useModal == undefined ? true : props.useModal,
            visible: false,
            items: props.items || [],
            multiple: props.multiple || false,
            selectedItem: null,
            selectedItems: [],
            selectedText: '',
            errors: []
        }
    }

    componentDidMount() {
        this.initSelectedItem(this.props.value);
    }

    componentWillReceiveProps(props) {
        this.initSelectedItem(props.value)
    }

    initSelectedItem(value) {
        if (this.props.multiple)
            this.setState({
                selectedItems: value
            })
        else
            this.setState({
                selectedItem: value
            }, () => this.setDisplayText())
    }

    trigger() {
        debugger
        this.setState({
            visible: true
        });
    }

    toggleSelected(id, selected) {
        debugger
        if (selected)
            this.unselect(id);
        else
            this.select(id);
    }

    getDisplayText() {
        if (this.state.multiple) {

        }
        else {
            let selectedItems = this.state.items.filter(e => e[this.idKey] == this.state.selectedItem);
            return selectedItems.length > 0 ? selectedItems[0][this.textKey] : ''
        }
    }

    setDisplayText() {
        this.setState({
            selectedText: this.getDisplayText()
        })
    }

    select(id) {
        if (this.state.multiple) {
            let selectedItems = this.state.selectedItems;
            selectedItems.push(id);
            this.setState({
                selectedItems: selectedItems
            })
        }
        else {
            this.setState({
                selectedItem: id
            }, () => {
                this.setDisplayText();
                if (this.onChange)
                    this.onChange(id)
            })
        }
    }

    unselect(id) {
        if (!this.state.multiple)
            return;

        let deletedIndex = -1;
        this.state.selectedItems.forEach((value, index) => {
            if (value == id) {
                deletedIndex = index;
                return;
            }
        });
        if (deletedIndex != -1) {
            let selectedItems = this.state.selectedItems;
            selectedItems.splice(deletedIndex, 1);
            this.setState({
                selectedItems: selectedItems
            })
        }
    }

    isSelected(id) {
        let self = this;
        if (this.state.multiple)
            return this.state.selectedItems.length > 0 ? this.state.selectedItems.filter(e => e == id).length > 0 : false;
        else
            return this.state.selectedItem == id;
    }

    getStyle() {
        return {
            borderBottomColor: this.state.errors.length == 0 ? color.formControlBorderColor : color.danger,
        }
    }

    render() {
        var self = this;
        return (
            <View style={{ flex: 1, padding: 10, borderWidth: 0.5, ...this.getStyle() }}>
                {this.state.useModal ? <TouchableOpacity style={styles.displayItem} onPress={this.trigger.bind(this)}>
                    <Left>
                        <View>
                            <Text>{this.state.title}</Text>
                            {this.state.selectedText ? <Text style={styles.chosen}>{this.state.selectedText}</Text> : null}
                        </View>
                    </Left>
                    <Right>
                        {this.renderIcon instanceof Function ? this.renderIcon() :
                            <Icon name="angle-down" type="FontAwesome" style={{ color: color.faded }}></Icon>}
                    </Right>
                </TouchableOpacity> : null}

                {!this.state.useModal ? <>
                    {this.state.items.map(item => {
                        let selected = self.isSelected(item[self.idKey]);
                        return (
                            <ListItem>
                                <TouchableOpacity style={styles.choice}
                                    onPress={() => self.toggleSelected(item[self.idKey], selected)}>
                                    <Left>
                                        <View>
                                            <Text>{item[self.textKey]}</Text>
                                            {item[self.subtitleKey] ? <Text style={styles.subtitle}>{item[self.subtitleKey]}</Text> : null}
                                        </View>
                                    </Left>
                                    {selected ? <Right>
                                        <Icon name="checkbox-marked" type="MaterialCommunityIcons" style={styles.iconSelected}></Icon>
                                    </Right> : null}
                                    {!selected ? <Right>
                                        <Icon name="checkbox-blank-outline" type="MaterialCommunityIcons" style={styles.iconSelected}></Icon>
                                    </Right> : null}
                                </TouchableOpacity>
                            </ListItem>
                        )
                    })}
                </> : null}

                {(this.state.visible && this.state.useModal) ? <Modal
                    animationIn='fadeIn'
                    animationOut='fadeOut'
                    isVisible={true}
                    style={styles.modal}
                    onBackButtonPress={() => { this.setState({ visible: false }) }}
                    onBackdropPress={() => { this.setState({ visible: false }) }}>

                    <ScrollView
                        style={{ flex: 1 }}
                        contentContainerStyle={styles.modalContainer}>
                        <View style={styles.modalHeader}>
                            <Text style={styles.modalTitle}>{this.state.title}</Text>
                        </View>
                        <List
                            keyExtractor={(e) => e[self.idKey]}>
                            {this.state.items.map(item => {
                                let selected = self.isSelected(item[self.idKey]);
                                return (
                                    <ListItem>
                                        <TouchableOpacity style={styles.choice} onPress={() => self.toggleSelected(item[self.idKey], selected)}>
                                            <Left><Text>{item[self.textKey]}</Text></Left>
                                            {selected ? <Right>
                                                <Icon name="checkbox-marked" type="MaterialCommunityIcons" style={styles.iconSelected}></Icon>
                                            </Right> : null}
                                            {!selected ? <Right>
                                                <Icon name="checkbox-blank-outline" type="MaterialCommunityIcons" style={styles.iconSelected}></Icon>
                                            </Right> : null}
                                        </TouchableOpacity>
                                    </ListItem>
                                )
                            })}
                        </List>
                        <View style={styles.cancelButtonContainer}>
                            <TouchableOpacity onPress={() => this.setState({ visible: false })}>
                                <Icon type="Ionicons" name="ios-close-circle-outline" style={styles.cancelButton}></Icon>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>

                </Modal> : null}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    modal: {
        paddingTop: 50,
        paddingBottom: 50,
    },
    displayItem: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    choice: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    modalContainer: {
        padding: 10,
        backgroundColor: 'white'
    },
    iconSelected: {
        color: '#3f51b5'
    },
    cancelButtonContainer: {
        paddingTop: 15,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    cancelButton: {
        color: '#444',
        fontSize: 32
    },
    modalHeader: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
    modalTitle: {
        fontSize: 18
    },
    chosen: {
        fontSize: 12,
        color: '#666'
    },
    subtitle: {
        fontSize: 12,
        color: '#666'
    }
});

export default ListArrayField;