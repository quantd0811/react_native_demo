import React from 'react';
import { View, StyleSheet, TextInput } from 'react-native';
import { Item, Input, Text, Label } from 'native-base';
import { color, labelForm } from '../../utils/variables';

class TextField extends React.Component {
    constructor(props) {
        super(props);
        this.onValueChange = props.onValueChange;
        this.state = {
            name: props.name,
            value: props.value,
            errors: [],
            valid: true,
            validators: props.validators || {},
        }
    }

    componentWillReceiveProps(props) {
        this.setState({
            value: props.value
        })
    }

    valid() {
        return this.state.errors.length == 0;
    }

    validate() {
        debugger
        let valid = true;
        let errors = [];

        if (this.state.validators.required) {
            if (this.state.value == '' || this.state.value == undefined || this.state.value == null) {
                errors.push('Vui lòng nhập ' + this.state.name);
                valid = false;
            }
        }

        this.setState({
            valid: valid,
            errors: errors
        })
    }

    getStyle() {
        return {
            borderBottomColor: this.state.errors.length == 0 ? color.formControlBorderColor : color.danger,
            marginBottom: 5,
            paddingLeft: 5,
            paddingRight: 5,
        }
    }

    render() {
        debugger
        return (
            <View>
                <Text style={{ ...labelForm, paddingLeft: 10, paddingTop: 10 }}>{this.state.name}</Text>
                {/* <Item style={this.getStyle()} inlineLabel> */}
                {/* <Label>{this.state.name}</Label> */}
                <Item style={this.getStyle()}>
                    <TextInput onBlur={() => this.validate()}
                        onChangeText={value => this.setState({ value: value })}
                        value={this.state.value} placeholder={this.state.name}
                        style={{
                            fontSize: 13,
                            paddingTop: 0,
                            paddingBottom: 0,
                            flex: 1
                        }} />
                </Item>
                {/* </Item> */}
                {this.state.errors.map(error => (
                    <Text style={styles.errorMessageColor}>{error}</Text>
                ))}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    errorMessageColor: {
        color: color.danger,
        fontSize: 13,
        paddingLeft: 5
    }
})

export default TextField;