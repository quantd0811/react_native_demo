import React from "react";
import {
  Modal,
  View,
  Platform,
  DatePickerIOS,
  DatePickerAndroid,
  TouchableOpacity
} from "react-native";
import { Text, Button, Icon } from "native-base";
import variable from "../../../../native-base-theme/variables/platform";
import moment from 'moment';
import { color, labelForm } from '../../utils/variables';

export default class DatePicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: props.name,
      modalVisible: false,
      defaultDate: props.defaultDate,
      chosenDate: typeof (props.value) == "string" ? moment(props.value, "MM/DD/YYYYTHH:mm:ss").toDate() : props.value,
      chosenDateIOS: typeof (props.value) == "string" ? moment(props.value, "MM/DD/YYYYTHH:mm:ss").toDate() : props.value,
      placeHolderText: props.placeHolderText || "dd/MM/yyyy",
      minimumDate: props.minimumDate || new Date(1900, 1, 1),
      maximumDate: props.maximumDate || new Date(new Date().getFullYear(), 12, 31),
      androidMode: props.androidMode || "default",
      locale: props.locale || "vi",
      timeZoneOffsetInMinutes: props.timeZoneOffsetInMinutes || undefined,
      modalTransparent: props.modalTransparent || false,
      animationType: props.animationType || "fade",
      textStyle: props.textStyle || { color: "black", fontSize: 13 },
      placeHolderTextStyle: props.placeHolderTextStyle || { color: "#d3d3d3", fontSize: 13 },
    };
  }

  componentDidMount = () => {
    this.setState({
      defaultDate: this.props.defaultDate ? this.props.defaultDate : new Date()
    });
  };

  componentWillReceiveProps(props) {
    if (this.state.chosenDate !== props.value) {
      this.setState({
        chosenDate: typeof (props.value) == "string" ? moment(props.value, "MM/DD/YYYYTHH:mm:ss").toDate() : props.value
      });
    }
  }

  setDate(date) {

    if (this.props.onDateChange) {
      if (this.props.onDateChange(date) != 0) {
        this.setState({ chosenDate: new Date(date) });
      };
    } else {
      this.setState({ chosenDate: new Date(date) });
    }
  }

  showDatePicker() {
    if (Platform.OS === "android") {
      this.openAndroidDatePicker();
    } else {
      this.setState({
        chosenDateIOS: this.state.chosenDate ? this.state.chosenDate
          : this.state.defaultDate
      });
      this.setState({ modalVisible: true });
    }
  }

  async openAndroidDatePicker() {
    try {
      const newDate = await DatePickerAndroid.open({
        date: this.state.chosenDate
          ? this.state.chosenDate
          : this.state.defaultDate,
        minDate: this.state.minimumDate,
        maxDate: this.state.maximumDate,
        mode: this.state.androidMode
      });
      const { action, year, month, day } = newDate;
      if (action === "dateSetAction") {
        this.setDate(new Date(year, month, day));
      }
    } catch ({ code, message }) {
      console.warn("Cannot open date picker", message);
    }
  }

  selectDate(chosenDate) {
    this.setDate(chosenDate)
    // this.setState({
    //   chosenDate: !!chosenDate ? moment(chosenDate, "MM/DD/YYYYTHH:mm:ss").toDate() : new Date()
    // });
    this.setState({ modalVisible: false });
  }

  setChosenDate(chosenDate) {
    this.setState({ chosenDateIOS: chosenDate })
  }
  render() {
    const variables = this.context.theme
      ? this.context.theme["@@shoutem.theme/themeStyle"].variables
      : variable;
    return (
      <TouchableOpacity style={{
        width: '100%',
        borderBottomColor: color.formControlBorderColor,
        borderBottomWidth: 0.5,
        paddingLeft: 10,
        paddingBottom: 5,
        paddingTop: 5
      }}
        onPress={this.showDatePicker.bind(this)}>
        <View>
          <View style={{ width: '100%' }}>
            <Text style={labelForm}>{this.state.name}</Text>
            <View style={{ flex: 7, flexDirection: 'row', alignItems: 'center' }}>
              <Icon name="calendar-month" type="MaterialCommunityIcons"></Icon>
              <Text
                style={[
                  { padding: 0, marginLeft: 5, color: variables.datePickerTextColor },
                  this.state.chosenDate ? this.state.textStyle : this.state.placeHolderTextStyle
                ]}
              >
                {this.state.chosenDate
                  ? moment(this.state.chosenDate).format("DD/MM/YYYY")
                  : this.state.placeHolderText}
              </Text>
            </View>
            {/* <View style={{ flex: 1, justifyContent: 'center', alignItems:'flex-end'}}>
              <TouchableOpacity onPress={this.showDatePicker.bind(this)} style={{flex:1, justifyContent: 'center', alignItems:'flex-end'}}>
              <Icon style={{ fontSize: 18 }} type="FontAwesome" name="calendar"  />
              </TouchableOpacity>
            </View> */}

          </View>
          <View>
            <Modal
              animationType={this.state.animationType}
              transparent={this.state.modalTransparent} //from api
              visible={this.state.modalVisible}
              onRequestClose={() => { }}
            >
              <View style={{ flex: 1, flexDirection: 'column' }}>
                <View style={{ flex: 1 }}>
                </View>
                <DatePickerIOS
                  date={
                    this.state.chosenDateIOS
                      ? this.state.chosenDateIOS
                      : this.state.defaultDate
                  }
                  onDateChange={this.setChosenDate.bind(this)}
                  minimumDate={this.props.minimumDate}
                  maximumDate={this.props.maximumDate}
                  mode="date"
                  locale={this.state.locale}
                  timeZoneOffsetInMinutes={this.state.timeZoneOffsetInMinutes}
                  style={{ flex: 1 }}
                />
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-end' }}>
                  <View style={{ flexDirection: 'row', flex: 1, height: 50, justifyContent: 'space-evenly' }}>
                    <Button style={{ backgroundColor: "#ffffff" }} onPress={() => this.setState({ modalVisible: false })}>
                      <Text style={{ color: '#007aff', fontSize: 15 }}>CANCEL</Text>
                    </Button>
                    <Button style={{ backgroundColor: "#ffffff" }} onPress={() => {
                      this.selectDate(this.state.chosenDateIOS
                        ? this.state.chosenDateIOS
                        : this.state.defaultDate)
                    }}>
                      <Text style={{ color: '#007aff', fontSize: 15 }}>OK</Text>
                    </Button>
                  </View>
                </View>
              </View>
            </Modal>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}