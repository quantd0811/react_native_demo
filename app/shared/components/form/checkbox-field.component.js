import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Item, Input, Text, Label, Icon } from 'native-base';
import { color } from '../../utils/variables';
import { CheckBox } from 'react-native-elements';

class CheckboxField extends React.Component {
    constructor(props) {
        super(props);
        this.onValueChange = props.onValueChange;
        this.state = {
            name: props.name,
            value: props.value,
            errors: [],
            valid: true,
            validators: props.validators || {},
        }
    }

    componentWillReceiveProps(props) {
        this.setState({
            value: props.value
        })
    }

    valid() {
        return this.state.errors.length == 0;
    }

    validate() {
        debugger
        let valid = true;
        let errors = [];

        this.setState({
            valid: valid,
            errors: errors
        })
    }

    getStyle() {
        return {

        }
    }

    render() {
        debugger
        return (
            <View>
                <CheckBox
                    containerStyle={{ borderWidth: 0, backgroundColor: 'transparent', padding: 0 }}
                    title={this.state.name}
                    checked={this.state.value}
                    checkedIcon={<Icon name="checkbox-marked" type="MaterialCommunityIcons" style={styles.iconSelected}></Icon>}
                    uncheckedIcon={<Icon name="checkbox-blank-outline" type="MaterialCommunityIcons" style={styles.iconSelected}></Icon>}
                    onPress={() => this.setState({ value: !this.state.value })}
                />
                {this.state.errors.map(error => (
                    <Text style={styles.errorMessageColor}>{error}</Text>
                ))}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    errorMessageColor: {
        color: color.danger
    },
    iconSelected: {
        color: '#3f51b5'
    },
})

export default CheckboxField;