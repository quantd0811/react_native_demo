import React from 'react';
// import { Spinner } from 'native-base';
import Spinner from 'react-native-spinkit';
import { View } from 'react-native';
import { color } from '../utils/variables';

const LoadingIcon = (props) => (
    <View style={{
        flexDirection: 'row',
        justifyContent: 'center',
        ...props.style
    }}>
        <Spinner style={{ color: props.color || color.primary, width: props.size || 30 }} type="Circle" />
    </View>
)

export default LoadingIcon;