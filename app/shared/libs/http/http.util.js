const FETCH_TIMEOUT = 30000;
let didTimeOut = false;


// hàm xử lý lỗi trả về của http dựa theo mã lỗi. fix repone của fetch api
export const handleError = function (resolve, reject, res, response) {
    if (res.ok) {
        resolve(response);
    }
    reject(response);
}
// hàm warp fetch api dựa theo kiểu res trả về của fecth để paser data và trả về
export const httpBackend = function (input, init) {
    return new Promise(function (resolve, reject) {
        const timeout = setTimeout(function() {
            didTimeOut = true;
            reject(new Error('Request timed out'));
        }, FETCH_TIMEOUT);


        fetch(input, init).then(res => {
            clearTimeout(timeout);
            didTimeOut = false;
            let contentType = res.headers.get('content-type');
            if(contentType != null)
            {
                if(contentType.indexOf("application/json") >= 0)
                {
                    contentType = 'application/json'
                }
            }

            
            switch (contentType) {
                case 'application/json':
                    res.json().then(response => {
                        handleError(resolve, reject, res, response)
                    })
                    break;
                default:
                    res.text().then(response => {
                        handleError(resolve, reject, res, response)
                    })
                    break;
            }
        }).catch(err => {
            if(didTimeOut) return;
            reject(err)
        })
    });
}

export function jsonToUrlencoded(obj) {
    const str = [];
    for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
            str.push(encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]));
        }
    }
    return str.join('&');
}