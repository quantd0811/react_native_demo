import HttpClient from './http.client';
import { API_URL } from '../../../core/app.setting';
export const Http = new HttpClient({
    apiUrl: API_URL
})
