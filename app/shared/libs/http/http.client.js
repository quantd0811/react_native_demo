import 'rxjs';
import { from } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { tap, map, catchError } from 'rxjs/operators';
import { httpBackend, jsonToUrlencoded } from './http.util';
import { store } from '../../../redux/store';


export default class HttpClient {
    constructor(otp) {
        this.apiUrl = otp.apiUrl
    }
    //function xu ly khi goi 1 request http
    request(method, url, options = {
        cache: 'no-cache',
        headers: new Headers({ 'content-type': 'application/json' }),
    }) {
        const initOption = Object.assign({}, {
            cache: options.cache || 'no-cache',
            headers: options.headers || new Headers({ 'content-type': 'application/json' }),
            method: method || 'GET',
            body: options.body || null,
            timeout: 30000,
        })

        let req = new Request(url, initOption);
        req = this.updateRequest(req)
        console.log(req)
        return from(httpBackend(req))
    }

    get(url, options = {
        cache: 'no-cache',
        headers: new Headers({ 'content-type': 'application/json' })
    }) {
        return this.request('GET', url, options)

    }

    post(url, body, options = {
        cache: 'no-cache',
        headers: new Headers({ 'content-type': 'application/json' })
    }) {
        body = this.processBody(body, options)
        options = Object.assign(options, {
            body: body
        })
        return this.request('POST', url, options)
    }
    put(url, body, options = {
        cache: 'no-cache',
        headers: new Headers({ 'content-type': 'application/json' })
    }) {
        body = this.processBody(body, options)
        options = Object.assign(options, {
            body: body
        })
        return this.request('PUT', url, options)
    }
    patch(url, body, options = {
        cache: 'no-cache',
        headers: new Headers({ 'content-type': 'application/json' })
    }) {
        body = this.processBody(body, options)
        options = Object.assign(options, {
            body: body
        })
        return this.request('PATCH', url, options)
    }
    delete(url, options = {
        cache: 'no-cache',
        headers: new Headers({ 'content-type': 'application/json' })
    }) {
        return this.request('DELETE', url, options)
    }
    //
    processBody(body, options) {
        switch (options.headers.get('content-type')) {
            case 'application/x-www-form-urlencoded':
                return jsonToUrlencoded(body);
            case 'application/json':
                return JSON.stringify(body);
            default:
                return body;
        }
        return body
    }
    //phương thức update lại url kèm theo api url
    updateUrl(url) {
        if (url.indexOf('http') !== -1) {
            return url;
        } else {
            return this.apiUrl + url;
        }

    }
    //phương thức cập nhật lại headers và url của request
    updateRequest(req) {
        const token = store.getState().loginReducer.token;
        let access_token = '';
        if (token) {
            access_token = `Token ${token}`;
        }

        // chỉnh sửa lại request để add thêm token vào header, chỉnh sửa lại url của request
        if (req.headers.get('Authorization')) {
            access_token = req.headers.get('Authorization');
        }
        const authReq = req.clone({
            headers: req.headers.set('Authorization', access_token)
        });
        authReq.url = this.updateUrl(req.url)
        return authReq;
    }

}