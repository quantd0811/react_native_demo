export const color = {
    primary: '#3f51b5',
    danger: '#cc0000',
    // faded: '#808080',
    faded: '#b3b3b3',
    formControlBorderColor: '#bfbfbf'
}

export const labelForm = {
    color: color.faded,
    fontSize: 13,
    marginBottom: 5
}