import { Alert } from 'react-native';

export function Confirm(message, title = "Thông báo") {
    var self = this;
    self.okCallBack = null;
    self.cancelCallBack = null;
    self.ok = function (fn) {
        if (fn instanceof Function)
            self.okCallBack = fn;
    }
    self.cancel = function (fn) {
        if (fn instanceof Function)
            self.cancelCallBack = fn;
    }

    Alert.alert(
        title,
        message,
        [
            {
                text: 'Hủy', style: 'cancel', onPress: () => {
                    debugger
                    if (self.cancelCallBack)
                        self.cancelCallBack();
                }
            },
            {
                text: 'Đồng ý', onPress: () => {
                    debugger
                    if (this.okCallBack)
                        self.okCallBack()
                }
            },
        ],
        { cancelable: true },
    );
}

export function AlertBox(message, title = "Thông báo") {
    Alert.alert(
        title,
        message,
        [
            {
                text: 'OK', onPress: () => { }
            },
        ],
        { cancelable: true },
    );
}