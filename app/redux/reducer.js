import { combineReducers } from 'redux';
import loginReducer from './login/login.reducer';

const rootReducer = combineReducers({   
    loginReducer
})

export default rootReducer;