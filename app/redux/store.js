import { persistReducer, persistStore } from 'redux-persist';
import { createEpicMiddleware } from 'redux-observable';
import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducer';
import storage from 'redux-persist/lib/storage';
import epics from './epic';

const persistReducerConfig = {
    key: 'root',
    storage: storage,
    debounce: 10000
}

const epicMiddleware = createEpicMiddleware();
const appPersistReducer = persistReducer(persistReducerConfig, rootReducer);

export const store = createStore(appPersistReducer, applyMiddleware(epicMiddleware));
epicMiddleware.run(epics);
export const appPersistStore = persistStore(store);