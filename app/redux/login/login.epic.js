import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs';
import { LOGIN } from './login.constant';
import { Http } from '../../shared/libs/http';
import { API_ENDPOINTS } from '../../core/app.setting';
import { loginSuccess, loginFail } from './login.action';
import { ofType } from 'redux-observable';
import { mergeMap, map, catchError } from 'rxjs/operators';

export const loginEpic = action$ => action$.pipe(
    ofType(LOGIN),
    mergeMap(action => {
        let body = {
            username: action.username,
            password: action.password
        }
        debugger
        return Http.post(API_ENDPOINTS.AUTH.LOGIN, body)
            .pipe(
                map(res => loginSuccess(res)),
                catchError(err => of(loginFail(err)))
            )
    })
);

