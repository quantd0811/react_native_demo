import { LOGIN, LOGOUT, LOGIN_FAIL, LOGIN_SUCCESS, CLEAR_ERROR } from './login.constant';

const loginState = {
    token: '',
    username: '',
    error: null,
    isLoggingIn: false
}

const loginReducer = (state = loginState, action) => {
    switch (action.type) {
        case LOGIN:
            return Object.assign({}, state, {
                isLoggingIn: true,
                username: action.username
            })
        case LOGOUT:
            return Object.assign({}, state, {
                token: '',
                username: '',
                error: null,
                isLoggingIn: false
            })
        case LOGIN_SUCCESS:
            return Object.assign({}, state, {
                isLoggingIn: false,
                token: action.token,
                error: null
            });
        case LOGIN_FAIL:
            return Object.assign({}, state, {
                isLoggingIn: false,
                error: action.error
            });
        case CLEAR_ERROR:
            return Object.assign({}, state, {
                error: null
            });
        default:
            return state;
    }
}

export default loginReducer;