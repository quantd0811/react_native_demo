// import * as actions from './model.action';
import * as constants from './model.constant';

const modelState = {
    item: null,
    items: [],
    total: 0,
    error: null,
    loading: false
}

export default modelReducer = (state = modelState, action) => {
    switch (action.type) {
        case constants.MODEL_SAVE:
            return Object.assign({}, state, {
                loading: true
            })
        case constants.MODEL_SAVE_SUCCESS:
            return Object.assign({}, state, {
                loading: false,
                error: null
            })
        case constants.MODEL_SAVE_FAIL:
            return Object.assign({}, state, {
                loading: false,
                error: action.error
            })
        case constants.MODEL_GET_ALL:
            return Object.assign({}, state, {
                loading: true
            })
        case constants.MODEL_GET_ALL_SUCCESS:
            return Object.assign({}, state, {
                loading: false,
                error: null,
                items: action.items,
                total: action.total
            })
        case constants.MODEL_GET_ALL_FAIL:
            return Object.assign({}, state, {
                loading: false,
                error: action.error
            })
        case constants.MODEL_GET_SINGLE:
            return Object.assign({}, state, {
                loading: true
            })
        case constants.MODEL_GET_SINGLE_SUCCESS:
            return Object.assign({}, state, {
                loading: false,
                error: null,
                item: action.item
            })
        case constants.MODEL_GET_SINGLE_FAIL:
            return Object.assign({}, state, {
                loading: false,
                error: action.error
            })
        case constants.MODEL_DELETE:
            return Object.assign({}, state, {
                loading: true
            })
        case constants.MODEL_DELETE_SUCCESS:
            return Object.assign({}, state, {
                loading: false,
                error: null
            })
        case constants.MODEL_DELETE_FAIL:
            return Object.assign({}, state, {
                loading: false,
                error: action.error
            })

        default: return state;
    }
}
