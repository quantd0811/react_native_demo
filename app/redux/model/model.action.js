import * as constant from './model.constant';

export function save(model) {
    return {
        type: constant.MODEL_CREATE,
        model: model
    }
}

export function saveSuccess(success) {
    return {
        type: constant.MODEL_SAVE_SUCCESS,
        success: success
    }
}

export function saveFail(error) {
    return {
        type: constant.MODEL_SAVE_FAIL,
        error: error
    }
}

export function getAll(page, pageSize) {
    return {
        type: constant.MODEL_GET_ALL,
        page: page,
        pageSize: pageSize
    }
}

export function getAllSuccess(items, total) {
    return {
        type: constant.MODEL_GET_ALL_SUCCESS,
        items: items,
        total: total
    }
}

export function getAllFail(error) {
    return {
        type: constant.MODEL_GET_ALL_FAIL,
        error: error
    }
}

export function getSingle(id) {
    return {
        type: constant.MODEL_GET_SINGLE,
        id: id
    }
}

export function getSingleSuccess(item) {
    return {
        type: constant.MODEL_GET_SINGLE_SUCCESS,
        item: item
    }
}

export function getSingleFail(error) {
    return {
        type: constant.MODEL_GET_SINGLE_FAIL,
        error: error
    }
}


export function deleteModel(id) {
    return {
        type: constant.MODEL_DELETE,
    }
}


export function deleteModelFail(error) {
    return {
        type: constant.MODEL_DELETE_FAIL,
        error: error
    }
}


export function deleteModelSuccess(success) {
    return {
        type: constant.MODEL_DELETE_SUCCESS,
        success: success
    }
}

