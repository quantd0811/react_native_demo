import { combineEpics } from 'redux-observable';
import { loginEpic } from './login/login.epic';

export default epics = combineEpics(
    loginEpic
)