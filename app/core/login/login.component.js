import React from 'react';
import { LoginTemplate } from './login.template.js';
import { connect } from 'react-redux';
import { login, loginSuccess, loginFail, clearError } from '../../redux/login/login.action';
import { Toast } from 'native-base';
import { Http } from '../../shared/libs/http';
import { API_URL } from '../app.setting';
import { Keyboard } from 'react-native';
import { AlertBox } from '../../shared/utils/custom-alert';

class LoginComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            loading: false
        }
        this._bootstrapApp(this.props);
    }

    componentWillReceiveProps(props) {
        this._bootstrapApp(props);
    }

    _bootstrapApp(props) {
        if (props.token) {
            this.props.navigation.navigate('Authorized');
        }

        if (props.error) {
            new AlertBox('Sai tài khoản hoặc mật khẩu');
            props.clearError();
        }

        this.setState({
            loading: props.isLoggingIn
        })
    }

    submit() {
        var self = this;
        if (this.state.username.trim() == '' || this.state.password.trim() == '') {
            Toast.show({
                position: 'top',
                text: "Vui lòng nhập tài khoản và mật khẩu",
                buttonText: "Okay",
                duration: 3000
            });
            return;
        }
        let body = {
            username: this.state.username,
            password: this.state.password
        }
        Keyboard.dismiss();
        this.props.login(body.username, body.password);
    }

    validate() {
        if (!this.state.username || !this.state.password) {
            Toast.show({
                text: 'Vui lòng nhập username và password',
                buttonText: 'OK'
            });
        }
    }

    render() {
        return LoginTemplate.call(this);
    }
}

const mapStateToProps = (state) => {
    return {
        token: state.loginReducer.token,
        error: state.loginReducer.error,
        isLoggingIn: state.loginReducer.isLoggingIn
    }
}

export default connect(mapStateToProps, { login, loginSuccess, loginFail, clearError })(LoginComponent);