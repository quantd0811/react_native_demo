import React from 'react';
import { View, StyleSheet, KeyboardAvoidingView, SafeAreaView, Platform } from 'react-native';
import { Form, Input, Item, Button, Icon, Text, Thumbnail } from 'native-base';
import LoadingIcon from '../../shared/components/loading-icon';
import { BRAND_NAME } from '../app.setting.js';

export const LoginTemplate = function () {
    return (
        <SafeAreaView style={{ flex: 1 }}>
            <KeyboardAvoidingView behavior="padding" style={styles.container}
                keyboardVerticalOffset={
                    Platform.select({
                        ios: () => 0,
                        android: () => -200
                    })()}>
                <Form style={styles.form}>
                    <View style={styles.brandContainer}>
                        <Thumbnail style={styles.thumbnail} large source={require('../../../assets/images/logo.png')} />
                        <Text style={styles.brand}>{BRAND_NAME}</Text>
                    </View>
                    <Item>
                        <Icon active type="FontAwesome" name='user' />
                        <Input placeholder='Tên đăng nhập' value={this.state.username}
                            onChangeText={username => this.setState({ username: username })} />
                    </Item>
                    <Item>
                        <Icon active type="FontAwesome" name='suitcase' />
                        <Input placeholder='Mật khẩu'
                            secureTextEntry
                            value={this.state.password}
                            onChangeText={password => this.setState({ password: password })} />
                    </Item>
                    <Button style={styles.submitButton} onPress={this.submit.bind(this)}>
                        <Text>Đăng nhập</Text>
                    </Button>
                    {this.state.loading ? <LoadingIcon /> : null}
                </Form>
            </KeyboardAvoidingView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: 10,
        paddingTop: 20,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    thumbnail: {
        marginBottom: 10
    },
    form: {
        alignSelf: 'stretch',
        alignItems: 'center'
    },
    submitButton: {
        marginTop: 15,
        width: '50%',
        justifyContent: 'center'
    },
    brandContainer: {
        paddingBottom: 20,
        paddingRight: 10,
        paddingLeft: 10,
        alignItems: 'center'
    },
    brand: {
        color: '#1a1a1a',
        fontSize: 24,
        textAlign: 'center'
    }
});