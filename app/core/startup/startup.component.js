import React from 'react';
import { View, Dimensions, Image } from 'react-native';
import LoadingIcon from '../../shared/components/loading-icon';

class StartupComponent extends React.Component {
    constructor(props) {
        super(props);

        this.screen = {
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height,
        }
    }

    render() {
        return (
            <View style={{
                flex: 1, width: this.screen.width, height: this.screen.height,
                justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff'
            }}>
                <Image style={{}} source={require('../../../assets/images/logo.png')}></Image>
                <LoadingIcon style={{ marginTop: 15 }} />
            </View>
        )
    }
}

export default StartupComponent;