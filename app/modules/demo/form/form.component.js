import React from 'react';
import FormTemplate from './form.template';
import { connect } from 'react-redux';
import { Model } from '../model';
import { AlertBox } from '../../../shared/utils/custom-alert';

class FormComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            model: new Model(),
            demoDropdownArrayItems: [
                { id: '1', text: 'item 1' },
                { id: '2', text: 'item 2' },
                { id: '3', text: 'item 3' },
                { id: '4', text: 'item 3' },
                { id: '5', text: 'item 3' },
                { id: '6', text: 'item 3' },
                { id: '7', text: 'item 3' },
                { id: '8', text: 'item 3' },
                { id: '9', text: 'item 3' },
                { id: '9', text: 'item 3' },
                { id: '10', text: 'item 3' },
                { id: '11', text: 'item 3' },
                { id: '12', text: 'item 3' },
                { id: '13', text: 'item 3' },
                { id: '14', text: 'item 3' },
            ],
            radioItems: [
                { name: 'r1', value: '1' },
                { name: 'r2', value: '2' },
                { name: 'r3', value: '3' },
            ],
            dateControlValue: new Date(),
            sliderValue: 50
        }
        this.form = null;
    }

    render() {
        return FormTemplate.call(this);
    }
}

const mapStateToProps = state => {
    return {}
}

export default connect(mapStateToProps, {})(FormComponent);

