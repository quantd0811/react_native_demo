import React from 'react';
import { Container, Button, Text, Icon, Header, Left, Body, Right, Content, ListItem, Title } from 'native-base';
import { View, StyleSheet } from 'react-native';
import ListArray from '../../../shared/components/form/list-array.component';
import TextField from '../../../shared/components/form/text-field.component';
import CheckBoxField from '../../../shared/components/form/checkbox-field.component';
import RadioField from '../../../shared/components/form/radio-field.component';
import DateField from '../../../shared/components/form/date-field.component';
import ListServerField from '../../../shared/components/form/list-server.component';
import { color } from '../../../shared/utils/variables';
import LoadingIcon from '../../../shared/components/loading-icon';
import { Slider } from 'react-native-elements';

export default FormTemplate = function () {
    return (
        <Container>
            <Header>
                <Left>
                    <Button transparent onPress={() => { this.props.navigation.goBack() }}>
                        <Icon name="chevron-left" type="MaterialCommunityIcons" style={styles.headerIcon} />
                    </Button>
                </Left>
                <Body>
                    <Title>Demo Form</Title>
                </Body>
                <Right></Right>
            </Header>
            <Content>
                <ListArray
                    title={'demo combobox'}
                    renderIcon={() => (
                        <Icon name="angle-right" type="FontAwesome" style={{ color: color.faded }}></Icon>
                    )}
                    items={this.state.demoDropdownArrayItems}
                    value={'1'}
                    onChange={value => { }} />
                <TextField name={'text'} value={'demo text'} validators={{ required: true }}></TextField>
                <CheckBoxField name={'checkbox'} value={true}></CheckBoxField>
                <RadioField name={'radio'} value={'1'} items={this.state.radioItems}></RadioField>
                <DateField value={new Date()} name={'demo date'}></DateField>
                <Button full>
                    <Text>Normal Button</Text>
                </Button>
                <Button full>
                    <LoadingIcon color={'#f2f2f2'} size={24} />
                    <Text>Loading Button</Text>
                </Button>
                <Button full disabled={true}>
                    <Text>Disabled Button</Text>
                </Button>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Button iconLeft full style={{ flex: 1 }}>
                        <Icon name='home' />
                        <Text>Icon Button</Text>
                    </Button>
                    <Button iconLeft full success style={{ flex: 1 }}>
                        <Icon name='home' />
                        <Text>Icon Button</Text>
                    </Button>
                </View>
                <View style={{ flex: 1, alignItems: 'stretch', justifyContent: 'center' }}>
                    <Slider
                        value={this.state.sliderValue}
                        onSlidingComplete={value => this.setState({ sliderValue: value })}
                        maximumValue={100}
                        minimumValue={0}
                        step={10}
                        thumbStyle={{backgroundColor: color.primary}}
                    />
                    <Text>Value: {this.state.sliderValue}</Text>
                </View>
            </Content>
        </Container>
    )
}

const styles = StyleSheet.create({
    headerIcon: {
        fontSize: 24,
        color: '#fff'
    }
})