export class Model {
    id = {
        type: 'text',
        value: '',
        name: 'id',
        validators: {
            required: true
        }
    }
    name = {
        type: 'text',
        value: '',
        name: 'name',
        validators: {
            required: true
        }
    }
    active = {
        type: 'boolean',
        value: true,
        name: 'active',
    }
    list = {
        type: 'dropdown',
        value: '1',
        items: [
            { id: '1', text: 'Item 1' },
            { id: '2', text: 'Item 2' },
            { id: '3', text: 'Item 3' },
        ],
        name: 'list',
    }
}