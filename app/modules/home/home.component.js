import React from 'react';
import { HomeTemplate } from './home.template';

class HomeComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return HomeTemplate.call(this);
    }
}

export default HomeComponent;