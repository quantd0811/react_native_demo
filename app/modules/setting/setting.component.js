import React from 'react';
import { SettingTemplate } from './setting.template';

class SettingComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return SettingTemplate.call(this);
    }
}

export default SettingComponent;