import React from 'react';
import {ScrollView, Text, View, Image, StyleSheet, Alert, TouchableOpacity } from 'react-native';
import { Icon, Container } from 'native-base';
import { connect } from 'react-redux';
import { logout } from '../redux/login/login.action';

const SideBar = props => {
    if (!props.token)
        props.navigation.navigate('Login');

    this.onLogout = function (props) {
        Alert.alert(
            'Xác nhận',
            'Đăng xuất khỏi thiết bị?',
            [
                {
                    text: 'Hủy',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: 'Xác nhận', onPress: () => this.logout(props) },
            ],
            { cancelable: false },
        );
    }

    this.logout = function (props) {
        props.logout();
    }

    this.closeDrawer = function (props) {
        props.navigation.closeDrawer();
    }

    return (
        <Container>
            <View>
                <View style={styles.userInfoContainer}>
                    <Image style={styles.avatar} source={require('../../assets/images/user_default_boy.png')}></Image>
                    <Text style={styles.userName}>Administrator</Text>
                    <Icon style={styles.goToUserDetailIcon} name='angle-right' type='FontAwesome' />
                </View>
                <ScrollView>
                    <TouchableOpacity onPress={() => { this.closeDrawer(props), props.navigation.navigate('Home') }}
                        style={[styles.menu, { backgroundColor: 'rgba(255,255,255,0.3)', borderRadius: 5 }]}>
                        <Icon style={styles.iconmenu} type="MaterialCommunityIcons" name="home" />
                        <Text style={styles.menuText} type='h5White'>Trang chủ</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { this.closeDrawer(props), props.navigation.navigate('Form') }}
                        style={[styles.menu, { backgroundColor: 'rgba(255,255,255,0.3)', borderRadius: 5 }]}>
                        <Icon style={styles.iconmenu} type="FontAwesome" name="gears" />
                        <Text style={styles.menuText} type='h5White'>Form</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { this.closeDrawer(props), props.navigation.navigate('List') }}
                        style={[styles.menu, { backgroundColor: 'rgba(255,255,255,0.3)', borderRadius: 5 }]}>
                        <Icon style={styles.iconmenu} type="FontAwesome" name="gears" />
                        <Text style={styles.menuText} type='h5White'>List</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.onLogout.call(this, props)}
                        style={[styles.menu, { backgroundColor: 'rgba(255,255,255,0.3)', borderRadius: 5 }]}>
                        <Icon style={styles.iconmenu} type="MaterialCommunityIcons" name="logout-variant" />
                        <Text style={styles.menuText} type='h5White'>Đăng xuất</Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>
        </Container>
    )
}

const styles = StyleSheet.create({
    userInfoContainer: {
        padding: 15,
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    avatar: {
        width: 50,
        height: 50,
        borderRadius: 25
    },
    userName: {
        marginLeft: 15,
        marginRight: 'auto'
    },
    menu: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderTopWidth: 1,
        borderTopColor: '#F0F0F0',
    },
    menuText: {
        marginLeft: 20,
        fontSize: 14,
        width: 150
    },
    iconmenu: {
        color: '#333333',
        fontSize: 24,
        width: 25
    }
});

const mapStateToProps = (state) => {
    return {
        token: state.loginReducer.token,
        username: state.loginReducer.username,
    }
}

export default connect(mapStateToProps, { logout })(SideBar);