import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import SideBar from './sidebar.component';
import { Platform } from 'react-native';

import HomeModule from '../modules/home/home.component';
import SettingModule from '../modules/setting/setting.component';

// demo only
import FormModule from '../modules/demo/form/form.component';
// import ListModule from '../modules/demo/list/list.component';
// end demo only

const AuthorizeNavigator = createStackNavigator({
    Home: {
        screen: HomeModule
    },
    Setting: {
        screen: SettingModule
    },
    // demo only
    Form: {
        screen: FormModule
    },
}, {
    headerMode: 'none',
    initialRouteName: 'Form'
});

export const AppDrawerNavigator = createDrawerNavigator({
    Home: AuthorizeNavigator
}, {
    contentComponent: props => <SideBar {...props} />
});