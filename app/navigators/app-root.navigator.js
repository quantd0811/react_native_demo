import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import LoginModule from '../core/login/login.component';
import { AppDrawerNavigator } from './app-authorized.navigator'

const MainNavigator = createSwitchNavigator({
    'Login': {
        screen: LoginModule
    },
    'Authorized': {
        screen: AppDrawerNavigator
    }
}, {
    'initialRouteName': 'Login',
});

const App = createAppContainer(MainNavigator);
export default App;