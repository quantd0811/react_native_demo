/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import {
    Header,
    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, appPersistStore } from './app/redux/store';
import AppRoot from './app/navigators/app-root.navigator';
import { StyleProvider, Root } from 'native-base';
import getTheme from './native-base-theme/components';
import platform from './native-base-theme/variables/platform';
import { StatusBar } from 'react-native';
import StartupComponent from './app/core/startup/startup.component.js';

export default App = function () {
    return (
        <Provider store={store}>
            <PersistGate loading={<StartupComponent></StartupComponent>} persistor={appPersistStore}>
                <StyleProvider style={getTheme(platform)}>
                    <Root>
                        <StatusBar barStyle="light-content" />
                        <AppRoot />
                    </Root>
                </StyleProvider>
            </PersistGate>
        </Provider>
    );
}